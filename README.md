# smart-app-rate

#### 项目简介

- Smart app rate是用户对应用的评分对话框，如果用户对应用程序的评级低于定义的阈值评级，则该对话框将变为反馈窗体，否则，它会将用户带到指定网页地址。

#### 功能说明

- 自动获取应用的图标显示在对话框的上方
- 设置session值来决定对话框的出现
- 如果评分低于threshold值则展示反馈表格
- 可自定义标题和按钮的文字
- 可自定义按钮文字和背景颜色
- 可自定义跳转网站地址

#### 功能演示

- 反馈功能效果：

  + ![gif](https://gitee.com/archermind-ti/smart-app-rate/raw/master/preview/smart-app-rate.gif)

#### 集成说明

- 方式一

下载smart-app-rate源码，启动 DevEco Studio并打开本工程，可直接编译运行entry示例工程。

- 方式二

在project的build.gradle中添加mavenCentral()的引用

```
repositories {   
 	...   
 	mavenCentral()   
	 ...           
 }
```

在entry的build.gradle中添加依赖

```
dependencies { 
... 
implementation 'com.gitee.archermind-ti:smart-app-rate:1.0.0-beta' 
... 
}
```

#### 使用说明

- 创建

对话框创建方式如下：

```java

final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                .threshold(3)
                .session(7)
                .onRatingBarFormSubmit(new RatingDialog.Builder.RatingDialogFormListener() {
                    @Override
                    public void onFormSubmitted(String feedback) {

                    }
                }).build();

        ratingDialog.show();

```

或者在创建时对相关属性进行设置：

```java
final RatingDialog ratingDialog = new RatingDialog.Builder(this)
                        .icon(pixelMap)
                        .session(7)
                        .threshold(3)
                        .title("How was your experience with us?")
                        .titleTextColor(ResourceTable.colorPrimaryDark)
                        .positiveButtonText("Not Now")
                        .negativeButtonText("Never")
                        .positiveButtonTextColor(ResourceTable.colorPrimary)
                        .negativeButtonTextColor(ResourceTable.colorPrimaryDark)
                        .formTitle("Submit Feedback")
                        .formHint("Tell us where we can improve")
                        .formSubmitText("Submit")
                        .formCancelText("Cancel")
                        .ratingBarColor(getResourceManager().getElement(ResourceTable.Color_yellow).getColor())
                        .playstoreUrl("YOUR_URL")
                        .onThresholdCleared(new RatingDialog.Builder.RatingThresholdClearedListener() {
                                           @Override
                                           public void onThresholdCleared(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {
                                               //do something
                                               ratingDialog.remove();
                                           }
                                       })
                        .onThresholdFailed(new RatingDialog.Builder.RatingThresholdFailedListener() {
                                           @Override
                                           public void onThresholdFailed(RatingDialog ratingDialog, float rating, boolean thresholdCleared) {
                                               //do something
                                               ratingDialog.remove();
                                           }
                                       })
                        .onRatingChanged(new RatingDialog.Builder.RatingDialogListener() {
                                            @Override
                                            public void onRatingSelected(float rating, boolean thresholdCleared) {

                                            }
                                       })
                        .onRatingBarFormSubmit(new RatingDialog.Builder.RatingDialogFormListener() {
                            @Override
                            public void onFormSubmitted(String feedback) {

                            }
                        }).build();

                ratingDialog.show();
```

- 注意点

  + 如果你想在点击事件中显示对话框，不用使用 `session()` 
  + 如果你不想显示反馈表单，不使用 `threshold()` 
  + 使用 `onThresholdCleared()` 重写默认的重定向流程
  + 使用 `onThresholdFailed()` 设置自定义的反馈表单流程

#### 版本迭代

- v1.0.0-beta

#### 版权和许可信息

```
Copyright (C) 2016 Code My Brains Out

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```