package com.codemybrainsout.rating;

import com.codemybrainsout.ratingdialog.RatingDialog;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import org.junit.Test;

import static org.junit.Assert.*;

public class RatingDialogTest {

    @Test
    public void testBuilder() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertNotNull(builder);
    }

    @Test
    public void testSession() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.session(2), builder);
    }

    @Test
    public void testThreshold() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.threshold(3), builder);
    }

    @Test
    public void testTitle() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.title("title"), builder);
    }

    @Test
    public void testIcon() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        PixelMap.InitializationOptions opts = new PixelMap.InitializationOptions();
        opts.pixelFormat = PixelFormat.ARGB_8888;
        opts.size = new Size(800, 800);
        PixelMap pixelMap = PixelMap.create(opts);
        assertSame(builder.icon(pixelMap), builder);
    }

    @Test
    public void testPositiveButtonText() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.positiveButtonText("PositiveButton"), builder);
    }

    @Test
    public void testNegativeButtonText() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.negativeButtonText("NegativeButton"), builder);
    }

    @Test
    public void testTitleTextColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.titleTextColor(ResourceTable.Color_colorPrimaryDark), builder);
    }

    @Test
    public void testPositiveButtonTextColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.positiveButtonTextColor(ResourceTable.Color_colorPrimaryDark), builder);
    }

    @Test
    public void testNegativeButtonTextColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.negativeButtonTextColor(ResourceTable.Color_colorPrimaryDark), builder);
    }

    @Test
    public void testPositiveButtonBackgroundColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.positiveButtonBackgroundColor(ResourceTable.Color_colorPrimaryDark), builder);
    }

    @Test
    public void testNegativeButtonBackgroundColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.negativeButtonBackgroundColor(ResourceTable.Color_colorPrimaryDark), builder);
    }

    @Test
    public void testOnThresholdCleared() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.onThresholdCleared((ratingDialog, rating, thresholdCleared) -> {}), builder);
    }

    @Test
    public void testOnThresholdFailed() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.onThresholdFailed((ratingDialog, rating, thresholdCleared) -> {}), builder);
    }

    @Test
    public void testOnRatingChanged() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.onRatingChanged((rating, thresholdCleared) -> {}), builder);
    }

    @Test
    public void testOnRatingBarFormSubmit() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.onRatingBarFormSubmit(feedback -> {}), builder);
    }

    @Test
    public void testFormTitle() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.formTitle("formTitle"), builder);
    }

    @Test
    public void testFormHint() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.formHint("formHint"), builder);
    }

    @Test
    public void testFormSubmitText() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.formSubmitText("formSubmit"), builder);
    }

    @Test
    public void testFormCancelText() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.formCancelText("formCancel"), builder);
    }

    @Test
    public void testRatingBarColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.ratingBarColor(Color.BLUE.getValue()), builder);
    }

    @Test
    public void testRatingBarBackgroundColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.ratingBarBackgroundColor(Color.DKGRAY.getValue()), builder);
    }

    @Test
    public void testFeedbackTextColor() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.feedbackTextColor(ResourceTable.Color_colorPrimaryDark), builder);
    }

    @Test
    public void testPlaystoreUrl() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertSame(builder.playstoreUrl("your custom url"), builder);
    }

    @Test
    public void testBuild() {
        RatingDialog.Builder builder = new RatingDialog.Builder(AbilityDelegatorRegistry.getAbilityDelegator()
                .getCurrentAbilitySlice(AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility()));
        assertNotNull(builder.build());
    }
}