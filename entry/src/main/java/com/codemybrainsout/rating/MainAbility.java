package com.codemybrainsout.rating;

import com.codemybrainsout.ratingdialog.RatingDialog;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

public class MainAbility extends Ability {

    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0, "MainAbility");
    private static final int TEST_NUMBER = 3;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Component rate = findComponentById(ResourceTable.Id_rlRate);
        rate.setClickedListener(component -> {
            HiLog.debug(LOG_LABEL, "onClick() ");
            showDialog();
        });
    }

    private void showDialog() {
        try {
            RatingDialog ratingDialog = new RatingDialog.Builder(getContext()).session(TEST_NUMBER).threshold(TEST_NUMBER)
                    .ratingBarColor(getResourceManager().getElement(ResourceTable.Color_yellow).getColor())
                    .playstoreUrl("https://" + "YOUR_URL")
                    .onRatingBarFormSubmit(feedback -> HiLog.debug(LOG_LABEL,"Feedback:" + feedback))
                    .build();
            ratingDialog.show();
        } catch (NotExistException | WrongTypeException | IOException e) {
            HiLog.error(LOG_LABEL, e.getMessage());
        };
    }
}
