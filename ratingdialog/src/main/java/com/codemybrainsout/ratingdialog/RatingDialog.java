package com.codemybrainsout.ratingdialog;

import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import java.io.IOException;

public class RatingDialog extends CommonDialog implements Rating.RatingChangedListener, Component.ClickedListener {

    private static final HiLogLabel LOG_LABEL = new HiLogLabel(HiLog.LOG_APP, 0, "RatingDialog");
    private static final String SESSION_COUNT = "session_count";
    private static final String SHOW_NEVER = "show_never";
    private static final String MY_PREFS = "RatingDialog";
    private static final int DEFAULT_DURATION = 300;
    private static final int DEFAULT_PADDING_LEFT = 20;
    private static final float DEFAULT_SHAKE_VALUE = 0.5f;
    private final Context context;
    private final Builder builder;
    private Text tvTitle, tvNegative, tvPositive, tvFeedback, tvSubmit, tvCancel;
    private Rating ratingBar;
    private Image ivIcon;
    private TextField etFeedback;
    private DirectionalLayout ratingButtons, feedbackButtons;
    private final float threshold;
    private final int session;

    private static Element getTintElement(Element element, int intColor) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[2][];
        states[0] = new int[]{1};
        states[1] = new int[]{-1};
        element.setStateColorList(states, colors);
        element.setStateColorMode(BlendMode.SRC_ATOP);
        return element;
    }

    public RatingDialog(Context ctx, Builder b) {
        super(ctx);
        this.context = ctx;
        this.builder = b;

        this.session = builder.session;
        this.threshold = builder.threshold;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Component dialogLayout = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dialog_rating, null, false);
        setContentCustomComponent(dialogLayout);
        tvTitle = (Text) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_title);
        tvNegative = (Text) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_button_negative);
        tvPositive = (Text) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_button_positive);
        tvFeedback = (Text) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_feedback_title);
        tvSubmit = (Text) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_button_feedback_submit);
        tvCancel = (Text) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_button_feedback_cancel);
        ratingBar = (Rating) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_rating_bar);
        ivIcon = (Image) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_icon);
        etFeedback = (TextField) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_feedback);
        ratingButtons = (DirectionalLayout) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_buttons);
        feedbackButtons = (DirectionalLayout) dialogLayout.findComponentById(ResourceTable.Id_dialog_rating_feedback_buttons);
        try {
            init();
        } catch (NotExistException | WrongTypeException | IOException e) {
            HiLog.error(LOG_LABEL, e.getMessage());
        }
    }

    @Override
    public void onClick(Component view) {
        if (view.getId() == ResourceTable.Id_dialog_rating_button_negative) {
            remove();
            showNever();
        } else if (view.getId() == ResourceTable.Id_dialog_rating_button_positive) {
            remove();
        } else if (view.getId() == ResourceTable.Id_dialog_rating_button_feedback_submit) {
            String feedback = etFeedback.getText().trim();
            HiLog.debug(LOG_LABEL, "onClick() feedback:" + feedback);
            if (feedback == null || feedback.length() == 0) {
                AnimatorValue animatorValue = new AnimatorValue();
                animatorValue.setDuration(DEFAULT_DURATION);
                animatorValue.setLoopedCount(2);
                animatorValue.setCurveType(Animator.CurveType.BOUNCE);
                float startX = etFeedback.getContentPositionX();
                animatorValue.setValueUpdateListener((animatorValue1, v)
                        -> etFeedback.setPosition((int) (startX + DEFAULT_PADDING_LEFT * (DEFAULT_SHAKE_VALUE - v)),
                        (int) etFeedback.getContentPositionY()));
                animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {}
                    @Override
                    public void onStop(Animator animator) {}
                    @Override
                    public void onCancel(Animator animator) {
                        etFeedback.setPosition((int) startX, (int) etFeedback.getContentPositionY());
                    }
                    @Override
                    public void onEnd(Animator animator) {
                        etFeedback.setPosition((int) startX, (int) etFeedback.getContentPositionY());
                    }
                    @Override
                    public void onPause(Animator animator) {}
                    @Override
                    public void onResume(Animator animator) {}
                });
                animatorValue.start();
                return;
            }
            if (builder.ratingDialogFormListener != null) {
                builder.ratingDialogFormListener.onFormSubmitted(feedback);
            }
            remove();
            showNever();
        } else if (view.getId() == ResourceTable.Id_dialog_rating_button_feedback_cancel) {
            remove();
        }
    }

    @Override
    public void onProgressChanged(Rating rating, int i, boolean b) {
        HiLog.debug(LOG_LABEL, "onProgressChanged() score:" + rating.getScore());
        boolean thresholdPassed;
        if (ratingBar.getScore() >= threshold) {
            thresholdPassed = true;
            if (builder.ratingThresholdClearedListener == null) {
                setRatingThresholdClearedListener();
            }
            builder.ratingThresholdClearedListener.onThresholdCleared(this, ratingBar.getScore(), thresholdPassed);

        } else {
            thresholdPassed = false;

            if (builder.ratingThresholdFailedListener == null) {
                setRatingThresholdFailedListener();
            }
            builder.ratingThresholdFailedListener.onThresholdFailed(this, ratingBar.getScore(), thresholdPassed);
        }

        if (builder.ratingDialogListener != null) {
            builder.ratingDialogListener.onRatingSelected(ratingBar.getScore(), thresholdPassed);
        }
        showNever();
    }

    @Override
    public void onStartTrackingTouch(Rating rating) {}

    @Override
    public void onStopTrackingTouch(Rating rating) {}

    public Text getTitleTextView() {
        return tvTitle;
    }

    public Text getPositiveButtonTextView() {
        return tvPositive;
    }

    public Text getNegativeButtonTextView() {
        return tvNegative;
    }

    public Text getFormTitleTextView() {
        return tvFeedback;
    }

    public Text getFormSumbitTextView() {
        return tvSubmit;
    }

    public Text getFormCancelTextView() {
        return tvCancel;
    }

    public Image getIconImageView() {
        return ivIcon;
    }

    public Rating getRatingBarView() {
        return ratingBar;
    }

    @Override
    public void show() {

        if (checkIfSessionMatches(session)) {
            super.show();
        }
    }

    private void init() throws NotExistException, WrongTypeException, IOException {
        tvTitle.setText(builder.title);
        tvPositive.setText(builder.positiveText);
        tvNegative.setText(builder.negativeText);
        tvFeedback.setText(builder.formTitle);
        tvSubmit.setText(builder.submitText);
        tvCancel.setText(builder.cancelText);
        etFeedback.setHint(builder.feedbackFormHint);
        initColor();
        if (builder.iconPixelMap != null) {
            ivIcon.setPixelMap(builder.iconPixelMap);
        }
        ratingBar.setRatingChangedListener(this);
        tvPositive.setClickedListener(this);
        tvNegative.setClickedListener(this);
        tvSubmit.setClickedListener(this);
        tvCancel.setClickedListener(this);
        if (session == 1) {
            tvNegative.setVisibility(Component.HIDE);
        }
        setAutoClosable(true);
    }

    private void initColor() throws NotExistException, WrongTypeException, IOException {
        ResourceManager resourceManager = context.getResourceManager();
        tvTitle.setTextColor(builder.titleTextColor != 0 ? new Color(resourceManager.getElement(builder.titleTextColor).getColor())
                : Color.BLACK);
        if (builder.positiveTextColor != 0) {
            tvPositive.setTextColor(new Color(resourceManager.getElement(builder.positiveTextColor).getColor()));
        }
        tvNegative.setTextColor(builder.negativeTextColor != 0 ? new Color(resourceManager.getElement(builder.negativeTextColor).getColor())
                : Color.LTGRAY);
        tvFeedback.setTextColor(builder.titleTextColor != 0 ? new Color(resourceManager.getElement(builder.titleTextColor).getColor())
                : Color.LTGRAY);
        if (builder.positiveTextColor != 0) {
            tvSubmit.setTextColor(new Color(resourceManager.getElement(builder.positiveTextColor).getColor()));
        }
        tvCancel.setTextColor(builder.negativeTextColor != 0 ? new Color(resourceManager.getElement(builder.negativeTextColor).getColor())
                : Color.LTGRAY);
        if (builder.feedBackTextColor != 0) {
            etFeedback.setTextColor(new Color(resourceManager.getElement(builder.feedBackTextColor).getColor()));
        }
        ShapeElement shapeElement = new ShapeElement();
        if (builder.positiveBackgroundColor != 0) {
            shapeElement.setRgbColor(RgbColor.fromArgbInt(resourceManager.getElement(builder.positiveBackgroundColor).getColor()));
            tvPositive.setBackground(shapeElement);
            tvSubmit.setBackground(shapeElement);
        }
        if (builder.negativeBackgroundColor != 0) {
            shapeElement.setRgbColor(RgbColor.fromArgbInt(resourceManager.getElement(builder.negativeBackgroundColor).getColor()));
            tvNegative.setBackground(shapeElement);
            tvCancel.setBackground(shapeElement);
        }
        initRatingBar();
    }

    private void initRatingBar() {
        VectorElement filledElement = new VectorElement(context, ResourceTable.Graphic_ic_rating_star_filled);
        if (builder.ratingBarColor != 0) {
            ratingBar.setFilledElement(getTintElement(filledElement, builder.ratingBarColor));
        } else {
            ratingBar.setFilledElement(filledElement);
        }
        VectorElement unfilledElement = new VectorElement(context, ResourceTable.Graphic_ic_rating_star_unfilled);
        if (builder.ratingBarBackgroundColor != 0) {
            ratingBar.setUnfilledElement(getTintElement(unfilledElement, builder.ratingBarBackgroundColor));
        } else {
            ratingBar.setUnfilledElement(unfilledElement);
        }
    }

    private void setRatingThresholdClearedListener() {
        builder.ratingThresholdClearedListener = (ratingDialog, rating, thresholdCleared) -> {
            remove();
            openPlayStore(context);
        };
    }

    private void setRatingThresholdFailedListener() {
        builder.ratingThresholdFailedListener = (ratingDialog, rating, thresholdCleared) -> openForm();
    }

    private void openForm() {
        tvFeedback.setVisibility(Component.VISIBLE);
        etFeedback.setVisibility(Component.VISIBLE);
        feedbackButtons.setVisibility(Component.VISIBLE);
        ratingButtons.setVisibility(Component.HIDE);
        ivIcon.setVisibility(Component.HIDE);
        tvTitle.setVisibility(Component.HIDE);
        ratingBar.setVisibility(Component.HIDE);
    }

    private void openPlayStore(Context ctx) {
        HiLog.debug(LOG_LABEL, "openPlayStore() url:" + builder.playstoreUrl);
        Intent intent = new Intent();
        Intent.OperationBuilder operationBuilder = new Intent.OperationBuilder();
        operationBuilder.withAction(IntentConstants.ACTION_SEARCH)
                .withUri(Uri.parse(builder.playstoreUrl));
        intent.setOperation(operationBuilder.build());
        ctx.startAbility(intent, 0);
    }

    private boolean checkIfSessionMatches(int sessionNumber) {
        HiLog.debug(LOG_LABEL, "checkIfSessionMatches() session:" + sessionNumber);
        if (sessionNumber == 1) {
            return true;
        }
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Preferences preferences = databaseHelper.getPreferences(MY_PREFS);
        if (preferences.getBoolean(SHOW_NEVER, false)) {
            return false;
        }
        int count = preferences.getInt(SESSION_COUNT, 1);
        HiLog.debug(LOG_LABEL, "checkIfSessionMatches() count:" + count);
        if (sessionNumber == count) {
            preferences.putInt(SESSION_COUNT, 1);
            preferences.flush();
            return true;
        } else if (sessionNumber > count) {
            count++;
            preferences.putInt(SESSION_COUNT, count);
            preferences.flush();
            return false;
        } else {
            preferences.putInt(SESSION_COUNT, 2);
            preferences.flush();
            return false;
        }
    }

    private void showNever() {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Preferences preferences = databaseHelper.getPreferences(MY_PREFS);
        preferences.putBoolean(SHOW_NEVER, true);
        preferences.flush();
    }

    public static class Builder {

        private final Context context;
        private String title, positiveText, negativeText, playstoreUrl;
        private String formTitle, submitText, cancelText, feedbackFormHint;
        private int positiveTextColor, negativeTextColor, titleTextColor, ratingBarColor, ratingBarBackgroundColor, feedBackTextColor;
        private int positiveBackgroundColor, negativeBackgroundColor;
        private RatingThresholdClearedListener ratingThresholdClearedListener;
        private RatingThresholdFailedListener ratingThresholdFailedListener;
        private RatingDialogFormListener ratingDialogFormListener;
        private RatingDialogListener ratingDialogListener;
        private PixelMap iconPixelMap;
        private int session = 1;
        private float threshold = 1;

        public Builder session(int sessionNumber) {
            this.session = sessionNumber;
            return this;
        }

        public Builder threshold(float value) {
            this.threshold = value;
            return this;
        }

        public Builder title(String dialogTitle) {
            this.title = dialogTitle;
            return this;
        }

        public Builder icon(PixelMap pixelMap) {
            this.iconPixelMap = pixelMap;
            return this;
        }

        public Builder positiveButtonText(String text) {
            this.positiveText = text;
            return this;
        }

        public Builder negativeButtonText(String text) {
            this.negativeText = text;
            return this;
        }

        public Builder titleTextColor(int textColor) {
            this.titleTextColor = textColor;
            return this;
        }

        public Builder positiveButtonTextColor(int textColor) {
            this.positiveTextColor = textColor;
            return this;
        }

        public Builder negativeButtonTextColor(int textColor) {
            this.negativeTextColor = textColor;
            return this;
        }

        public Builder positiveButtonBackgroundColor(int backgroundColor) {
            this.positiveBackgroundColor = backgroundColor;
            return this;
        }

        public Builder negativeButtonBackgroundColor(int backgroundColor) {
            this.negativeBackgroundColor = backgroundColor;
            return this;
        }

        public Builder onThresholdCleared(RatingThresholdClearedListener listener) {
            this.ratingThresholdClearedListener = listener;
            return this;
        }

        public Builder onThresholdFailed(RatingThresholdFailedListener listener) {
            this.ratingThresholdFailedListener = listener;
            return this;
        }

        public Builder onRatingChanged(RatingDialogListener listener) {
            this.ratingDialogListener = listener;
            return this;
        }

        public Builder onRatingBarFormSubmit(RatingDialogFormListener listener) {
            this.ratingDialogFormListener = listener;
            return this;
        }

        public Builder formTitle(String text) {
            this.formTitle = text;
            return this;
        }

        public Builder formHint(String formHint) {
            this.feedbackFormHint = formHint;
            return this;
        }

        public Builder formSubmitText(String text) {
            this.submitText = text;
            return this;
        }

        public Builder formCancelText(String text) {
            this.cancelText = text;
            return this;
        }

        public Builder ratingBarColor(int color) {
            this.ratingBarColor = color;
            return this;
        }

        public Builder ratingBarBackgroundColor(int color) {
            this.ratingBarBackgroundColor = color;
            return this;
        }

        public Builder feedbackTextColor(int color) {
            this.feedBackTextColor = color;
            return this;
        }

        public Builder playstoreUrl(String url) {
            this.playstoreUrl = url;
            return this;
        }

        public RatingDialog build() {
            return new RatingDialog(context, this);
        }

        public Builder(Context ctx) {
            this.context = ctx;
            // Set default PlayStore URL
            this.playstoreUrl = "market://details?id=" + context.getBundleName();
            initText();
        }

        private void initText() {
            title = context.getString(ResourceTable.String_rating_dialog_experience);
            positiveText = context.getString(ResourceTable.String_rating_dialog_maybe_later);
            negativeText = context.getString(ResourceTable.String_rating_dialog_never);
            formTitle = context.getString(ResourceTable.String_rating_dialog_feedback_title);
            submitText = context.getString(ResourceTable.String_rating_dialog_submit);
            cancelText = context.getString(ResourceTable.String_rating_dialog_cancel);
            feedbackFormHint = context.getString(ResourceTable.String_rating_dialog_suggestions);
        }

        public interface RatingThresholdClearedListener {
            void onThresholdCleared(RatingDialog ratingDialog, float rating, boolean thresholdCleared);
        }

        public interface RatingThresholdFailedListener {
            void onThresholdFailed(RatingDialog ratingDialog, float rating, boolean thresholdCleared);
        }

        public interface RatingDialogFormListener {
            void onFormSubmitted(String feedback);
        }

        public interface RatingDialogListener {
            void onRatingSelected(float rating, boolean thresholdCleared);
        }
    }
}
